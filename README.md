# Optimise box packing

Contest to find the maximum number of boxes that can be packed using given constraints

# Contest Description

One package has a size of 10 cm * 5 cm * 3.6 cm.

The forwarder accepts a belt dimension of max. 300 cm.

The belt dimension is calculated as follows: 2 * width + 2 * height + longest side.

The longest side must not exceed 175 cm.

What is the maximum number of packages that can be delivered?

Solve it in a python script. First working entry wins the contest.

P.S. The answer is 1386 boxes. You find a illustration here: https://i.ibb.co/N243Bt9/Illustration.png
