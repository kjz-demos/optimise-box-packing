# Box dimensions
box = {'l' : 10, 'w' : 5, 'h' : 3.6}
axes = ['x', 'y', 'z']
max_belt_dim = 300
max_arr_len = 175
arrangements = []
optimum = {'boxes': 0}

# Lists containing possible multiples of box dimensions
# mw = [5, 10, 15, 20, ..., 175]
ml = list(range(box['l'], max_arr_len + 1, box['l']))
mw = list(range(box['w'], max_arr_len + 1, box['w']))
mh = [i/10 for i in list(range(int(box['h'] * 10), (max_arr_len + 1) * 10, int(box['h'] * 10)))] # Do this to all three in a general application

box_volume = box['w'] * box['l'] * box['h']

# Determine the two other sides
def other_sides (side):
    return list(set(['w', 'l', 'h']) - set([side]))

# Fill a space of given dimensions with boxes. The edge must be parallel to the length.
# One of the boxes orientations is already taken by the main arrangement. There are only two other orientations left.
def fill (l, w, h, edge):
    fitting, layers, flexible_sides = [], l // box[edge], other_sides(edge)
    box_area = box[flexible_sides[0]] * box[flexible_sides[1]]

    # Find the maximum possible boxes that can be side by side in a row within the w * h space
    smallest_flexible_side = min([box[flexible_sides[0]], box[flexible_sides[1]]])
    max_boxes_inline = max(w, h) // smallest_flexible_side
    
    # Try all possible combinations of the two orientations and store them in the `fitting` list
    for i in range(int(max_boxes_inline) + 1):
        # Remaining gap when part of the w * h space is filled with i rows
        # cm
        rem = [
            w - i * box[flexible_sides[0]],
            h - i * box[flexible_sides[1]]
        ]

        max_j = int(max(rem) // smallest_flexible_side)

        for j in range(max_j + 1):
            fit_width = [
                w // box[flexible_sides[0]],
                w // box[flexible_sides[1]]
            ]
            test_fit = int((i * fit_width[0] + j * fit_width[1]) * layers)

            if test_fit * box_area <= w * h and (i * box[flexible_sides[1]] + j * box[flexible_sides[0]]) <= h: # Skip a combination if it does not fit into the crossectional area
                fitting.append({
                    'boxes': test_fit,
                    'layers': layers,
                    'stacking': [[fit_width[0], i, layers], [fit_width[1], j, layers]],
                    'orientation': [[flexible_sides[0], flexible_sides[1], edge], [flexible_sides[1], flexible_sides[0], edge]]
                })

    fitting.sort(key = lambda x:x['boxes'])

    return fitting.pop()

def close_gaps (arrangement):
    most_boxes = arrangement['boxes']

    # Add the gap to the longest side. 
    # Because of a smaller cross-section, that's the one that has less losses due to remainders
    sides = arrangement['sides']
    p_side = arrangement['orientation'][2]
    boxes_per_layer = arrangement['stacking'][0] * arrangement['stacking'][1]
    long_side = arrangement['gap'] + sides[p_side]

    # Try mixing box orientations in a way that covers the most free space
    b = sides[p_side] // box[p_side]
    while b > 1:
        new_total = b * boxes_per_layer
        nt = int(new_total)
        for s in ['h', 'w', 'l']: # Check them in order of size
            remainder = long_side - b * box[p_side]
            more_layers = remainder // box[s]
            if more_layers:
                gap_filling = fill(remainder, sides[arrangement['orientation'][0]], sides[arrangement['orientation'][1]], s)
                new_total = nt + gap_filling['boxes']

            if new_total > most_boxes:
                arrangement['boxes'] = new_total
                arrangement['stacking'][2] = b
                arrangement['filling'] = gap_filling

        b -= 1

    return arrangement

for width in mw:
    for length in ml:
        for height in mh:
            sett = {'w':width, 'l':length, 'h':height}
            sett = dict(sorted(sett.items(), key = lambda x:x[1])) # Align the longest side with the z-axis
            orientation = list(sett.keys())
            sides = list(sett.values())
            volume = sides[0] * sides[1] * sides[2]
            bd = 2 * (sides[0] + sides[1]) + sides[2]
            if (  bd < 100 or bd > max_belt_dim ): # Arbitrarily chose 100cm as too small
                continue
            boxes = volume / box_volume
            arr = {
                'boxes': boxes,
                'gap': round(max_belt_dim - bd, 1),
                'orientation': orientation,
                'sides' : sett,
                'stacking' : [
                    sides[0]/box[orientation[0]],
                    sides[1]/box[orientation[1]],
                    sides[2]/box[orientation[2]]
                ]
            }
            arrangements.append(arr)
            if (boxes > optimum['boxes']):
                optimum = arr

large_volumes = arrangements[:]
large_volumes.sort(key = lambda x:x['boxes'])
large_volumes = large_volumes[-10:] # Choose top ten


for i, arr in enumerate(large_volumes):
    large_volumes[i] = close_gaps(arr)

    if large_volumes[i]['boxes'] > optimum['boxes'] :
        optimum = large_volumes[i]

optimum["filling"]["orientation"].insert(0, optimum["orientation"])
optimum["filling"]["stacking"].insert(0, optimum["stacking"])

# Describe the stacking
optimum_stacking = ''

for i, o1 in enumerate(optimum["filling"]["orientation"]):
    optimum_stacking += ' + ' if i > 0 else ''
    for j, o2 in enumerate(o1):
        optimum_stacking += '(' if j == 0 else ''
        optimum_stacking += f'{int(optimum["filling"]["stacking"][i][j])}{o2}'
        optimum_stacking += ')' if j == 2 else ' x '

print(f'Boxes: {optimum["boxes"]}')

print("""
In the coordinate system below, additional boxes are appended to the stacking along the the z-axis which always coincides with the longest side of the whole arrangements
  y
  ^
  |
  |
   ----> x
   \\
    \\
      z
""")

print(f'Stacking: {optimum_stacking}')
print(f'Where: h = {box["h"]}, l = {box["l"]} and w = {box["w"]}')
print('As shown in the contest diagram, there are at most three blocks of boxes with distinct orientations. The stacking above represents the three blocks')