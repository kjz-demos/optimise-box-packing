Hi Contest Holder,
Thank you very much for actually taking time to look at my code and giving feedback :)

That stacking is calculated in the first stages when the script is trying to find the arrangements that are most promising. At this stage, all the script is doing is taking all combinations of the multiples of the box dimensions and ignoring those whose belt dimensions are less than 100 or greater than 300. It then takes the top ten arrangements according to the number of boxes they carry. After that, it passes each one of the ten to the function `close_gaps()` that tries to make use of the "gaps" available. The gaps are just the difference between the arrangement's belt dimensions and the maximum allowed belt dimensions.

The `close_gaps()` function plays around with the last element in the stacking list, 27.0, in this case. It lowers that number by one and checks if the gap is big enough to fit in a box in a different orientation, and keeps doing that till it reaches 1. When the gap is big enough, it calls `fill()` with the dimensions of the gap and the identity of the side that's going to be parallel to the longest side of the whole thing. 

`fill()` tries different combinations of the two possible box orientations. Only those that don't result in bigger dimensions than allowed are stored in the fitting list. There are dozens of such combinations. When `fill()` is done, it returns the highest number of boxes it managed to fit into the gap it was given to fill. It does not return the packing of the boxes, but that can be done if that information is needed.

Because the stacking in the original arrangement has been changed by the two functions, it's no longer useful at the end of the program's execution. I did not return that information, because I was just building a script that finds what the maximum number is, but the code can easily be tweaked to spit out which final arrangement gave it the highest number of boxes.

If this explanation is not clear, I can share a more documented version of the code :)